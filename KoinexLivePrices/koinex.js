/*
  * Name: Koinex Live Prices
  * Author: Sai Karthik [kskarthik at disroot dot org]
  * License:GPLv3
*/
'use strict'

 // parses API from koinex.in
function loadPrices() {

    const url = "https://koinex.in/api/ticker"

 fetch(url, {mode: "cors"}).then(function(response) {
    
    return response.json();
    
  }).then(function(myJson) {
  
  JSON.stringify(myJson);
  
var json = myJson.prices.inr;

// loop through the json & list coin prices as table
for (var i in json) {

 let trow = document.createElement('tr');
  
    trow.innerHTML ="<td>" + i + "</td>" + "<td>" + json[i] + "</td>";
   
       document.querySelector('#table-body').appendChild(trow);
    }
  }).catch((err) => {
    if(typeof err === 'string') err = new Error(err)
    console.error(err)
  });
}

loadPrices(); 