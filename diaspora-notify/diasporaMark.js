let notify = document.querySelector("#notification-dropdown");
let read = document.querySelector("#mark-all-read-link");

notify.addEventListener("click", markRead);

function markRead() {
 
   read.click();
   read.textContent = "All read !";

}
